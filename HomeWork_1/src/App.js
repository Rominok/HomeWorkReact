import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MyData from './jsonData.json';

class ListItem  extends Component {
  state = {
    done: true
  }
  done = (event) => {
    let status = this.state.done === true ? false : true;
    this.setState({ done: status });
  }
  render = () => {

    return(
       <li className='list'>{this.props.item.name}</li>
    );
  }
};



class App extends Component {
  state = {
   click: false,
   myContacts: MyData
 }

 done = (event) => {
    console.log( event, this.state )
    let status = this.state.click === true ? false : true;
    this.setState({ click: status });
  }
  handleChange = (event) => {
    let searchQuery = event.target.value.toLowerCase();
    let renderContacts = MyData.filter( (item) => {
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchQuery) !== -1;
  });
    this.setState({ myContacts: renderContacts});
    console.log( renderContacts );
  }

  render() {
    let { myContacts } = this.state;
    console.log(myContacts);
    let u = [3];
    return (
      <div className="wrapper">
        <div className='header'>
          <h1>Список гостей</h1>
          <img className='getPoisk' src='http://www.myiconfinder.com/uploads/iconsets/c8c2a2224743fec679813aa5e8a9e515.png'  onClick={this.done}/>
          <div className={
             this.state.click === true ? "activ" : "disable"
           }>
            <input type='text' className='text' onChange={this.handleChange}/>
           </div>
        </div>

        <ul className='ul'>
          {
            myContacts.length !== 0 ?
                myContacts.map( (item, key) => {
                  return(
                    <ListItem key={key} item={item} />
                  );
                })
                :
                <div className="NotFind">We didnt find contacts from your search query!</div>
          }
        </ul>
      </div>
    );
  }
}


export default App;
