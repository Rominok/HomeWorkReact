import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Link, Route, BrowserRouter, Switch, NavLink} from 'react-router-dom';
const List = () => (
  <div>
      <Link to='/list/1'>Item_1</Link>
      <Link to='/list/2'>Item_2</Link>
  </div>
);
class Page extends Component{
  state = {
    loading: false,
    loaded: false,
    data: {},
    name: ''
  }

  componentDidMount(){
    let { match } = this.props;
    this.setState({
      loading: true
    })
    fetch(`https://jsonplaceholder.typicode.com/posts/${match.params.id}`)
    .then(response => response.json())
    .then(json => {
      console.log(json);
      this.setState({
        data: json,
        name: match.params.id,
        loading: false,
        loaded: true
      })
    })
  }

  render = () => {

    let {name, data, loading, loaded } = this.state;
    if( loading === true){
      return(<div>Loader</div>)
    }
    if( loaded === true ) {
      return(
        <div>
          <h1>{name}</h1>
          <p>
            {data.body}
          </p>
         </div>
      );
    }
    if( loaded === false && loading === false){
      return(<div>Placeholder</div>)
    }
  }
}

const About = () => (
  <div>
    <h1>About</h1>
  </div>
);
const Contacts = () => (
  <h1>+38068223212</h1>
)
class App extends Component {
  render() {


    return (
      <div className="App">
        <h1>Task_1</h1>

        <BrowserRouter>
          <div>
            <div>
              <NavLink exact to='/'>HomePage</NavLink>
              <NavLink to='/list'>List</NavLink>
              <NavLink to='/about'>About</NavLink>
              <NavLink to='/contacts'>Contacts</NavLink>
            </div>
            <Switch>
              <Route exact path="/"  render={ () => {
                return (<h1>Exact Route</h1>);}
              }/>
              <Route exact path="/list" component={List} />
              <Route exact path='/about' component={About} />
              <Route exact path='/contacts' component={Contacts} />
              <Route path="/list/:id" component={Page} />
              <Route render={()=>(
                  <div>
                    <h1>404</h1>
                  </div>
              )}/>
            </Switch>

          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
