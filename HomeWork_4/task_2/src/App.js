import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import {Page, List, Home} from './Page';
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <div className='menu'>
              <NavLink exact to='/'>HomePage</NavLink>
              <NavLink to='/actor'>Actor</NavLink>
            </div>
            <Switch>
              <Route exact path="/"  component={Home}/>
              <Route exact path="/actor" component={List}/>
              <Route exact path='/actor/:id' component={Page}/>
              <Route exact path='/actor/:id/:name' return={()=>(
                <div>
                  <h1>TEST</h1>
                </div>
              )}/>
              <Route render={()=>(
                  <div>
                    <h1>404</h1>
                  </div>
              )}/>
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
