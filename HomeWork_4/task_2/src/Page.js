import React, { Component } from 'react';
import {  Link } from 'react-router-dom';

const Home = () =>(
  <h1>Home</h1>
)

const List = () => (
  <div className='action'>
    <Link to='/actor/cuKKbBWWXm?indent=2'>Franc</Link>
    <Link to='/actor/bGyYXYiCOG?indent=2'>Vishu</Link>
    <Link to='/actor/bTUcoMILQO?indent=2'>Bin</Link>
  </div>
)
class Page extends Component {
  state = {
    data: {},
    loaded: false
  }
  componentDidMount(){
    let { match } = this.props;
    console.log(match.params.id)
    fetch(`http://www.json-generator.com/api/json/get/${match.params.id}`)
    .then(response => response.json())
    .then(data =>
      this.setState({
        data: data,
        loaded: true
      })
    )
  }
  render(){
    let { match } = this.props;
    let {loaded, data} = this.state;

    if(loaded === true){
      return(
        <div>
          <h1>Actor</h1>
          {console.log({data})}
          {
            data.map( (e, index)=>(
               <li key={index}>
                 <Link to={`/actor/${match}/${index}`}>
                    {e.name}
                  </Link>
                  Количество композиций {e.album.length}
                </li>
              )
            )
          }
        </div>
      )
    }else {
      return(<div>Placeholder</div>)
    }
  }
}

export {List, Page, Home};
