import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import Page from './Page'
class App extends Component {
  render() {
    return(
      <div className="start">
        <img className="App-logo" width="100" src={logo}/>
        <h1>HomeWork 6</h1>
        <div>
          <NavLink to="/">Home</NavLink>
          <Switch>
            <Route path="/" component={Page}/>
          </Switch>
        </div>
      </div>
    )
  }
}

export default App;
