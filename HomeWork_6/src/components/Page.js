import React, { Component } from 'react';
import { connect } from 'react-redux';

import Actions from '../actions/demoActionCreator';
class Page extends Component{

componentDidMount(){
  let { propsAddToDo, ToDo } = this.props;

  fetch('https://next.json-generator.com/api/json/get/EyVGzeiT4')
  .then(response => response.json())
  .then(data=>  propsAddToDo(data))
}

  render = () => {
    let { ToDo, propsAddToDo } = this.props;
    let { activ } = this;
    let {loading, data} = ToDo;
    if (loading == true){
      return(
        <div>
          {
           data.map((e, key)=>(

               <div key={key}>{e.name.first}</div>

           ))
          }
        </div>
      );
    }
  else {
    return(
      <div>
        loading
      </div>);
  }
}
}
// - - - - - - - -  -
const MapStateToProps = (state, ownProps) => {
  return {
    ToDo: state.Users
  }
}

const MapDispatchToProps = ( dispatch ) => {
  return {
    dispatch,
    propsAddToDo: ( data ) => {
      dispatch( Actions.addToDo(data) );
    }
  }
}

const ConnectedHome = connect(
  MapStateToProps,
  MapDispatchToProps
)(Page)

export default ConnectedHome;
