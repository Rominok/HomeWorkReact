import { combineReducers } from 'redux';


const usersInitialState = {
  loading: false,
  loaded: false,
  data: [],
  errors: []
};

function Users( state = usersInitialState, action){
  switch( action.type ){

    case 'REQUEST':
      return  {
        ... state,
        data: action.data,
        loading: true
      }

    case 'RESPONSE':
      return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };

    case 'ERROR':
      return {
        ...state,
        errors: action.error
       };

    default:
      return state;
  }
}


const reducer = combineReducers({
  Users
});

export default reducer;
