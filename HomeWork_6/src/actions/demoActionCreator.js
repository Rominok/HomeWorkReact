const demoActionCreator = {
  doSomsng: () => {
    return function (dispatch, getState) {
      dispatch({
          type: 'ACTION_TYPE',
          payload: ['some', 'data']
        });
    };
  },
  addToDo: (data) => {
    return ( dispatch, getState ) => {
      dispatch({
        type: "REQUEST",
        data: data
      })
    }
  }
};


export default demoActionCreator;
