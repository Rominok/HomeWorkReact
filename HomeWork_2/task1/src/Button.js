import React, { Component } from 'react';

const Button = ({title, style, action}) => (
  <button style={style} onClick={action}>
     {title}
  </button>
);
export default Button;
