import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Switch, Route, Link, NavLink } from 'react-router-dom';
import Home from './Page';
class App extends Component {

  render() {
    let { test } = this.props;
    return(
      <div className="start">
        <img className="App-logo" width="100" src={logo}/>
        <h1>Clean React-Redux Project with React Router HomeWork 5 <button onClick={test}>ADD!</button> </h1>
        <BrowserRouter>
          <div>
            <div>
              <NavLink exact to='/'>Все записи</NavLink>
              <NavLink to='/action'>Выполненые записи</NavLink>
              <NavLink to='/noaction'>Не выполненые записи</NavLink>

            </div>
            <Switch>
              <Route exact path='/' component={Home}/>
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    )
  }
}

export default App;
