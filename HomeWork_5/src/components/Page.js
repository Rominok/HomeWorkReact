import React, { Component } from 'react';
import { connect } from 'react-redux';

import Actions from '../actions/demoActionCreator';

const List = ({name}) => (
  <ul>
    <li>
      {name}
      <button>X</button>
    </li>
  </ul>
);
const CreateList = ({action, textHandler, value}) =>(
  <form>
    <input type='text' value={value} onChange={textHandler}/>
    <input type='button' value='Добавить' onClick={action}/>
  </form>
);

class Home extends Component {
  state = {
    newToDo : ''
  }


  action = () => {

    let { propsAddToDo } = this.props;
    let { newToDo } = this.state;
    propsAddToDo( newToDo );

  }
  textHandler = (element) => {
    console.log(  element.target.value )
    let value = element.target.value;
    this.setState({ newToDo: value })
  }
  render(){
      let { action, textHandler } = this;
      let { newToDo } = this.state;
      let { ToDo } = this.props;
      return(
        <div>
          <CreateList
            textHandler={textHandler}
            action={action}
            value={newToDo}
          />
          {
            ToDo.data.length != 0 ?
              ToDo.data.map( (e, key) => (
                <List key={key} name = {e}/>
              ))

             : <div>Empty</div>
          }

        </div>
      )
    }
  }
// - - - - - - - -  -
const MapStateToProps = (state, ownProps) => {
  return {
    ToDo: state.Data
  }
}

const MapDispatchToProps = ( dispatch ) => {
  return {
    dispatch,
    propsAddToDo: ( data ) => {
      dispatch( Actions.addToDo(data) );
    }
  }
}

const ConnectedHome = connect(
  MapStateToProps,
  MapDispatchToProps
)(Home)
export default ConnectedHome;
