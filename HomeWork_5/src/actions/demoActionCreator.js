const demoActionCreator = {
  doSomsng: () => {
    return function (dispatch, getState) {
      dispatch({
          type: 'ACTION_TYPE',
          payload: ['some', 'data']
        });
    };
  },
  addToDo: ( newData ) => {
    return ( dispatch, getState ) => {
      dispatch({
        type: "ADD",
        data: newData
      })
    }
  }
};

export default demoActionCreator;
