import { combineReducers } from 'redux';


const usersInitialState = {
  loading: false,
  loaded: false,
  data: [],
  errors: []
};
const initialState = {
  activ: true
};
const initialData = {
  data: [],
  value: ''
};

function Data(state = initialData, action){
  switch (action.type) {
    case 'ADD':
      return {
        ...state,
        data: [...state.data, action.data]
      }

      case 'REMOVE':
        return {
          ...state,
          data: state.data.splice(action.index-1, 1)
        }

    default:
      return state;
  }
}
function Test(state = initialState, action){
  switch( action.type){
    case 'ACTIV':
      return  {
        ... state,
        activ: true
      }
      case 'DISABLE':
        return {
          ... state,
          activ: false
        }

    default:
      return state;
  }
}
function Users( state = usersInitialState, action){
  switch( action.type ){

    case 'REQUEST':
      return  {
        ... state,
        loading: true
      }

    case 'RESPONSE':
      return {
        ...state,
        data: action.data,
        loading: false,
        loaded: true
      };

    case 'ERROR':
      return {
        ...state,
        errors: action.error
       };

    default:
      return state;
  }
}


const reducer = combineReducers({
  Users,
  Test,
  Data
});

export default reducer;
