import React, { Component } from 'react';
import './style/App.css';
import PropTypes from 'prop-types';
import {Toggler, TogglerItem} from './AdvancedChild/toggler';

const CustomInput = ({name, type, placeholder, value, handler}) => (
  <label>
    <div>{name}</div>
    <input
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={handler}
      data-name={value}
    />
  </label>

);
CustomInput.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
  placeholder: PropTypes.string.isRequired,
  handler: PropTypes.func.isRequired
};


class App extends Component {
  state = {
    textData: [],
    data: [
      {
        name: 'ИМЬЯ',
        type: 'text',
        placeholder:'ИМЬЯ'
      },
      {
        name: 'ПАРОЛЬ',
        type: 'password',
        placeholder:'ПАРОЛЬ'
      },
      {
        name: 'ВОЗРАСТ',
        type: 'number',
        placeholder:'ВОЗРАСТ'
      }
    ]

  }
  changeHandler = (e) => {
    console.log(e.target);
     let target = e.target;
     let name = target.dataset.name;
     let value = e.target.value;
     this.setState({

     })
   }

  render = () => {

    let { data} = this.state;
    let { changeHandler } = this;
    return(
      <div>
        <h1> Lesson 3:Task 3</h1>
        <form>
          {
            data.map((e, key)=>(
              <CustomInput
                key={key}
                name={e.name}
                type={e.type}
                placeholder={e.placeholder}
                handler={changeHandler}
              />
            ))
          }
          <Toggler>
            <TogglerItem
              name='F'
            />
            <TogglerItem
              name='M'
            />
          </Toggler>
        

        </form>
      </div>
    );
  }
}
/*

*/
export default App;
