import React, { Component } from 'react';
import './style/App.css';
import {Toggler, TogglerItem} from './AdvancedChild/toggler';


import UnControlled from './ControlledForm/uncontrolledDemo';
// import CompWithPropTypes from './PropTypes/';
// import ControlledForm from './ControlledForm';

import AdvancedChild from './AdvancedChild';

const ComponentToSend = () => (<div>Null</div>);


class App extends Component {

  render = () => {

    return(

      <div>
        <h1>HomeWork 3 Task 1</h1>
        <Toggler>
          <TogglerItem
            name='1'

          />
        </Toggler>
        <Toggler>
          <TogglerItem
            name='2'

          />
        </Toggler>
        <Toggler>
          <TogglerItem
            name='3'

          />
        </Toggler>

      </div>
    );
  }
}
/*

*/
export default App;
