import React, { Component } from 'react';



export class Toggler extends Component {
  state = {
    activeToggler: ''
  }
  action = (e)=>{
    let target = e.target;
    let value = target.dataset.value;
    this.setState({activeToggler: value});
  }
  render(){
    let {name, children} = this.props;
    let {activeToggler} = this.state;
    let {action} = this;
    return(
      <div>

        <div className="togglerContainer">
          {
            React.Children.map(

              children,
              (ChildrenItem) => {
                console.log('start', activeToggler);
                if(ChildrenItem.props.name === activeToggler){
                    return React.cloneElement(ChildrenItem, {
                      name: ChildrenItem.props.name,
                      active: true,
                      action: action
                    })
                } else {
                  console.log('stop');
                  return React.cloneElement(ChildrenItem, {
                    name: ChildrenItem.props.name,
                    action: action,
                    active: false
                  })
                }
              }
            )
            }
        </div>
      </div>
    );
  }
}

export const TogglerItem = ({name, active, action}) => {
  return(
    <div className={
      active === true ?
        "togglerItem active":
        "togglerItem native "
      }
      data-value={name}
      onClick={
        action
      }
      >
      {name}
    </div>
  );
};
