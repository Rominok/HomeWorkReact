import React, { Component } from 'react';
import './style/App.css';
import PropTypes from 'prop-types';



const CustomInput = ({name, type, placeholder, value, handler}) => (
  <label>
    <div>{name}</div>
    <input
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={handler}
    />
  </label>

);
CustomInput.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
  placeholder: PropTypes.string.isRequired,
  handler: PropTypes.func.isRequired
};


class App extends Component {
  state = {
    name: 'input',
    type: 'text',
    placeholder:'test',

  }
  changeHandler = (e) => {
     let value = e.target.value;
     this.setState({
       textDemo: value
     })
   }
  render = () => {
    let { name, type, placeholder } = this.state;
    let { changeHandler } = this;
    return(
      <div>
        <h1> Lesson 3:Task 2</h1>
        <CustomInput
          name={name}
          type={type}
          placeholder={placeholder}
          handler={changeHandler}
        />
      </div>
    );
  }
}
/*

*/
export default App;
