import React, { Component } from 'react';
/*

  React.Children ->
    Методы для примербора дочерных элементов

    React.Children.map(children, function[(thisArg)]) -> Возвращает новый массив
    React.Children.forEach(children, function[(thisArg)])

    React.Children.count(children)
    React.Children.only(children)


  React.isValidElement(object)

  React.cloneElement(
    element,
    [props],
    [...children]
  )
*/

class DemoChild extends Component{
  render = () => {
    let { children } = this.props;
    return(
      <div>
        {
          React.Children.map(
            children,
            ( childItem => {
              console.log( childItem );
              if( React.isValidElement(childItem) ){
                console.log('Valid Component', childItem);
              } else {
                console.warn('Invalid Component',childItem)
              }

            })
          )
        }
      </div>
    )
  }
}


export default DemoChild;
