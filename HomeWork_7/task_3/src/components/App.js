import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Helmet from 'react-helmet';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import Spinner from './Spinner.svg';
import ReduxForm from './reduxForm';

class App extends Component {
  componentDidMount(){
    let { getUsers } = this.props;
    getUsers();
  }
  render() {
    let { loaded, data } = this.props.users;

    if( loaded === true ){
      return (
        <div className="App">


          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">HomeWork 7</h1>
          </header>


          <Switch>

            <Route path="/" component={ReduxForm}/>
          </Switch>

        </div>
      );
    } else {
      return(<div className="wv"><img src={Spinner}/></div>);
    }
  }
}

export default App;
