import React from 'react';

class CustomInput extends React.Component {

  render = () => {
    let {
      input: {
        value,
        onChange
      }
    } = this.props;
    return(
      <div>
        <span>Current Value { value }</span>
        <button type="button" onClick={()=>onChange(+value +1)}> + </button>
        <button type="button" onClick={()=>onChange(value -1)}> - </button>
      </div>
    )}
}

export default CustomInput;
