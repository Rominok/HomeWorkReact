/*
  DOCS: https://redux-form.com/7.3.0/
*/
import React from 'react';
import Helmet from 'react-helmet';
import Spinner from './Spinner.svg';
import { reduxForm, Field, FormSection  } from 'redux-form';
import { connect } from 'react-redux';
import { formValues } from 'redux-form';
import CustomInput from './customInput';
const UpperCaseNormalize = value => value && value.toUpperCase();

class ReduxFormComponent extends React.Component{

  render = () => {
    let { handleSubmit, reset } = this.props;

      return(
        <div>
          <h1>Redux Form</h1>
          <form onSubmit={handleSubmit}>
            <Field name="firstName" component="input" normalize={UpperCaseNormalize} type="text"/>
            <Field name="lastName" component="input" type="text"/>
            <FormSection name="education">
              <Field name="university" component="input" type="text"/>
              <Field name="city" component="input" type="text"/>
              <Field name="country" component="input" type="text"/>
            </FormSection>
            <FormSection name="address">
              <Field name="country" component="input" type="text"/>
              <Field name="city" component="input" type="text"/>
              <Field name="street" component="input" type="text"/>
            </FormSection>
            <button type='submit'>Submit</button>
          </form>
        </div>
      )
  }
}

// -- Redux --
const mapStateToProps = (state, ownProps) => {
  return {
      users: state.users,
      initialValues: {
         firstName: '',
         lastName: '',
         education: {
           university: '',
           city: '',
           country: ''
         },
         address: {
           country: '',
           city: '',
           street: ''
         }

        }
      }
  };


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch
  };
};

const ConnectedForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: 'simpleForm',
    onSubmit: values => console.log('test', values)
  })(ReduxFormComponent)
);


export default ConnectedForm;
