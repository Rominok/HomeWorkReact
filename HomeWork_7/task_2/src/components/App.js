import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Spinner from './Spinner.svg';
import axios from 'axios';

class App extends Component {
  componentDidMount(){
    let { getUsers } = this.props;
    axios.get('https://next.json-generator.com/api/json/get/EyVGzeiT4')
    .then(response => getUsers(response))

  }
  render() {
    let { loaded, data } = this.props.users;

    if( loaded === true ){
      return (
        <div className="App">
          {
            data.map((element, key)=>(
              <div key={key}>{element.email}</div>
            ))
          }
        </div>
      );
    } else {
      return(<div className="wv"><img src={Spinner}/></div>);
    }
  }
}

export default App;
