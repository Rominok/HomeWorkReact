import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import {List, Page, Home} from './Home';
import Helmet from 'react-helmet';
class App extends Component {
  render() {
    return(
      <div className="start">
        <div className='menu'>
          <NavLink exact to='/'>HomePage</NavLink>
          <NavLink to='/actor'>Actor</NavLink>
        </div>
        <Switch>
          <Route exact path="/"  component={Home}/>
          <Route exact path="/actor" component={List}/>
          <Route exact path='/actor/:id' component={Page}/>
          <Route exact path='/actor/:id/:name' return={()=>(
            <div>
              <Helmet>
                <html lang="en" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <title>Albom</title>
              </Helmet>
              <h1>TEST</h1>
            </div>
          )}/>
          <Route render={()=>(
              <div>
                <Helmet>
                  <html lang="en" />
                  <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                  <title>Error</title>
                </Helmet>
                <h1>404</h1>
              </div>
          )}/>
        </Switch>
      </div>
    )
  }
}

export default App;
