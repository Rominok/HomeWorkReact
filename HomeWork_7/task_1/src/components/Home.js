import React, { Component } from 'react';
import {  Link } from 'react-router-dom';
import Helmet from 'react-helmet';
const Home = () =>(
  <div>
    <Helmet>
      <html lang="en" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <title>Home</title>
    </Helmet>
    <h1>Home</h1>
  </div>
)

const List = () => (
  <div className='action'>
    <Helmet>
      <html lang="en" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <title>List</title>
    </Helmet>
    <Link to='/actor/cuKKbBWWXm?indent=2'>Franc</Link>
    <Link to='/actor/bGyYXYiCOG?indent=2'>Vishu</Link>
    <Link to='/actor/bTUcoMILQO?indent=2'>Bin</Link>
  </div>
)
class Page extends Component {
  state = {
    data: {},
    loaded: false
  }
  componentDidMount(){
    let { match } = this.props;
    console.log(match.params.id)
    fetch(`http://www.json-generator.com/api/json/get/${match.params.id}`)
    .then(response => response.json())
    .then(data =>
      this.setState({
        data: data,
        loaded: true
      })
    )
  }
  render(){
    let { match } = this.props;
    let {loaded, data} = this.state;

    if(loaded === true){
      return(
        <div>
        <Helmet>

          <html lang="en" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <title>Actor</title>
        </Helmet>

          <h1>Actor</h1>
          {console.log({data})}
          {
            data.map( (e, index)=>(
               <li key={index}>
                 <Link to={`/actor/${match}/${index}`}>
                    {e.name}
                  </Link>
                  Количество композиций {e.album.length}
                </li>
              )
            )
          }
        </div>
      )
    }else {
      return(<div>Placeholder</div>)
    }
  }
}

export {List, Page, Home};
