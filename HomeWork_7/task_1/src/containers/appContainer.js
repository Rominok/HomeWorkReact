import { connect } from 'react-redux';

// IMPORT YOUR REACT COMPONENT
import App from '../components/App';
// IMPORT YOUR ACTIONS
import demoActionCreator from '../actions/demoActionCreator';

const mapStateToProps = (state, ownProps) => {
  return {
    SomeData: "SomeValue"
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    demoActionCreator: () => {
      dispatch( demoActionCreator.doSomsng() );
    }
  };
};

const MyApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default MyApp;
